package com.training;

import com.training.person.Person;
import com.training.pets.Dragon;

public class InterfacesMain  {
    public static void main(String[] args) {
        Feedable[] thingsWeCanFeed = new Feedable[5];

        Person myHorace = new Person("Horace");
        myHorace.setName("Horace O'Brien");

        Dragon mySmaug = new Dragon("Smaug");
        mySmaug.breatheFire();

        thingsWeCanFeed[0] = myHorace;
        thingsWeCanFeed[1] = mySmaug;

        for(int i = 0; i < thingsWeCanFeed.length; i++){
            if(thingsWeCanFeed[i] != null){
                thingsWeCanFeed[i].feed();
            }

            if(thingsWeCanFeed[i] instanceof Dragon){
                ((Dragon)thingsWeCanFeed[i]).breatheFire();
            }
        }
    }
}
