package com.training;

import com.training.person.Person;

public class PersonMain {

    public static void main(String[] args) {
        Person myPerson = new Person("Tim");
        System.out.println(myPerson);
    }
}
